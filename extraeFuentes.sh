#!/bin/sh
#	Extrae una lista de fuentes de un repo con base
#	una lista de archivos
#	por Filipe Cesar Brandao - 02/12/2019
#
#	como usar: 
#
#	1) Copie y pegue este scipt en la raiz donde están sus repos locales.
#
#	2) Cria un archivo fuentes.txt (formato UNIX LF) en la 
#	misma carpeta del script. Ese archivo debe contener una 
#	lista con la ruta completa de todas las fuentes que se van
#	copiar de su repo.
#
#	3)Ejecute el script extraeFuentes.sh & verifica que las
#	fuentes están presentes en un archivo que se llama
#	fuentes.zip El resumen del proceso de puede leer en 
#	salida.txt
#
#
#inicio script
#
date>salida.txt

#	cambia formato del archivo lista para UNIX LF

if dir |grep fuentes.txt; then
 dos2unix fuentes.txt
 echo "Archivo fuentes.txt convertido a UNIX/LF">>salida.txt
 else
 echo "Archivo fuentes.txt no encontrado">>salida.txt
fi

#	generar listado de archivos a seren comprimidos con 
if
 tar -cvf fuentes.zip -T fuentes.txt >>salida.txt
 then
	echo "Archivos comprimidos en fuentes.zip">>salida.txt
else
	echo "Uno o más archivos no fueron encontrados.">>salida.txt
fi
#
#	fin del script!
#!/bin/sh
#	Crea estrutura de carpetas y archivos de fuentes para solución de
#	confictos de forma manual
#	por Filipe Cesar Brandao - 02/12/2019
#
#	como usar: 
#
#	1) Copie y pegue este scipt en una carpeta cualquier.
#
#	2) Cria un archivo fuentes.txt (formato UNIX LF) en la 
#	misma carpeta del script. Ese archivo debe contener una 
#	lista con la ruta completa de todas las fuentes que estan 
#	en conflicto desde Bitbuket.
#
#	3) Crea una carpeta llamada fuentes, en mismo directorio
# 	del script. Copia y pega todas las fuentes en conflicto 
#	dentro de esta carpeta, sin los directorios originales.
#
#	4)Ejecute el copia.sh & verifica los resultados en 
#	el archivo salida.txt
#
#
#inicio script
#
ruta=""
date>salida.txt

#cambia formato del archivo lista para UNIX LF

 if find fuentes.txt -type f -print0 | xargs -0 dos2unix ;then
 echo "Archivo fuentes.txt convertido a UNIX/LF">>salida.txt
 else
 echo "Archivo fuentes.txt no encontrado">>salida.txt
fi

if
	while read archivo
	do
	ruta=${archivo%/*}
	fuente=./fuentes/${archivo##*/}
	mkdir -p ./$ruta
	if cp $fuente $ruta ; then
		echo "Copiado $fuente ----->./$ruta/${fuente##*/}">>salida.txt
	else
		echo " Archivo $fuente no encontrado.">>salida.txt
	fi
	done < ./fuentes.txt ; then
	echo "Fin!"
else
	echo "Archivo ./fuentes.txt no encontrado!">>salida.txt
fi
#
#	fin del script!
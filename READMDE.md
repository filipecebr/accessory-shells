# SHELLS DE APOYO A OPERACIONES CON REPOSITORIOS V.0.0.0

### Autor: Filipe Brandao
### Fecha creación: 30/12/2019

## 1. Contenido del repo

Las dos Shell que se encuentran en ese repositorio sirven para 
apoyo a operaciones con multiplas fuentes. Sirben para extrair automaticamente
fuentes selecionadas de un repositorio o extrutura de archivos y tambien 
replicar esas extruturas.

### 1.1 Shell copia.sh

Esta shell copia el contenido definido por lo usuario. El mismo debe estar
definido en un archivo texto local, obligatoriamente llamado *fuentes.txt*.
Este archivo debe se ubicar en el la misma carpeta donde esta la shell. Se
recomienda copiar y pegar 
